<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'boilermediajunkies_wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm$tEXmg@T+aduyfp;nJH19z%PS(JWu&j;9ucEle0pFh2U1%z_8m$>*>rpkyZQ BQ');
define('SECURE_AUTH_KEY',  'tT:S*eC Bgp*gM<R(v(jpUu:L/lqk9AKU {0EUKel{z5v;xc]HWiQn:](8t?WBn+');
define('LOGGED_IN_KEY',    '.jQhx9Bt7!RL+a]?9D/Mhc|PH[tORoom{Ypp9-u5MbO`)Kfts#^6kl6jzYAHj%Z;');
define('NONCE_KEY',        ')Uo$dhnJF;X&?o/%p|F6T~m#F.@aYAnFt!S%0Dz#iOigiU3:P7%]8HYPED0/H4(x');
define('AUTH_SALT',        'JkjO{aEeKes-Y~[n]?(T=~Zcj9%p|FDQ7p*:9.lxp]r-T3]P0_2zRjhDQO~*0}}]');
define('SECURE_AUTH_SALT', 'l$_32,oD$xq6JWYYhTa=UB> t]3K{q;&9R59K*qHi/+{7Fx)4%l4fF25*QPKUg@.');
define('LOGGED_IN_SALT',   'x@h6OzjG|*krlMBnbbHf~OylNwv<L<V)!T}`tAyOU<)jPPhMdQhKwW.K>SlN#JoB');
define('NONCE_SALT',       '/^T/lWjo]?A>*I`9PMSNg0@F0q0>5M(3#IbP<b._4@;?dT[au1b6,ITk=WHj~`cL');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
