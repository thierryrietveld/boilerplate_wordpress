const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const jshint = require('gulp-jshint');
const uglify = require('gulp-uglify');
let cleanCSS = require('gulp-clean-css');
 
// gulp.task('default', () =>
//   gulp.src('/res/sass/*.scss')
//     .pipe(autoprefixer())
//     .pipe(concat('all.css'))
//     .pipe(gulp.dest('./dist/'))
// );

gulp.task('default', ['sass', 'minify-css', 'lint']);

gulp.task('sass', () => {
  gulp.watch('./res/sass/**/*.scss', () => {
    return gulp.src('./res/sass/**/*.scss')
          .pipe(autoprefixer())
          .pipe(concat('style.css'))
          .pipe(sass().on('error', sass.logError))
          .pipe(gulp.dest('./css'));
  })
})

gulp.task('minify-css', () => {
  return gulp.src('css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('css'));
});

gulp.task('lint', function() {
  return gulp.src('./res/scripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});